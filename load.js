const rp = require('request-promise');
var https = require('https');  

class LoadZillow{
    constructor(){     
        this.headers = {'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
        //'accept-encoding': 'gzip, deflate, sdch, br',
        'accept-language': 'en-GB,en;q=0.8,en-US;q=0.6,ml;q=0.4',
        'cookie': 'zguid=23|%24917cefa9-1483-4104-b710-99b0323a7526; zgsession=1|60b831e5-9b52-4a79-a15b-5f8eb913d0ef; _ga=GA1.2.248125237.1569313922; _gid=GA1.2.1416099955.1569313922; zjs_user_id=null; zjs_anonymous_id=%22917cefa9-1483-4104-b710-99b0323a7526%22; JSESSIONID=6A1059F4AF125B4ECFF526D8D4DFA4C0; _gcl_au=1.1.1266799511.1569313925; KruxPixel=true; DoubleClickSession=true; KruxAddition=true; ki_r=; __gads=ID=0c7a4715cda2ba55:T=1569314047:S=ALNI_MaOZ0qPww_oH5eVwDDTht_bOEHkSQ; _pxff_tm=1; ki_s=199445%3A0.0.0.0.2; zgcus_aeuut=AEUUT_2ed3ca7e-dea6-11e9-bba8-0e440c55d788; zgcus_aeut=AEUUT_2ed3ca7e-dea6-11e9-bba8-0e440c55d788; _csrf=Kl2e6wMP_N_PrDujMts-Iqp3; __stripe_sid=3a6499e5-8b00-4c06-8951-fe86cba8d9f1; __stripe_mid=66ca17aa-96d4-4a8d-b853-ca54b7036069; FSsampler=1424989923; ki_t=1569313965806%3B1569313965806%3B1569314016662%3B1%3B2; ZG_SW_REGISTERED=true; rental_csrftoken=97uGAPPm-VGLYTa26ZnUu3b0QxS3PdpvdSK0; swVersion=0.0.339; _gat=1; _px3=6a6f9ab8fad37bbb518348dbbee0fe1b288fc8607a7cc4b69a430c67f444e750:nEC8yKy1V65hvn0xogp1zQuHCWK43W7JtDGGociA63/0YiRuqNoEg6E1qWfDKlnpOvBMnBAdBfFS9xVM7tKJIQ==:1000:pSfmgmwzPBRIv3F5pTnWr0N9U98/MHaSi1rqC/B7VcEhHVd3JpUng1wdHtnia6aNSG3i7J+vTScAaHEgk1vKK8aSVGLPsaJcsylZ77LEksT49uF0YGEPvyFbeb7u9iDO1KPavwSE4EePHFSW6WplGZh6V6m0AnkPbYaqzcRdOek=; _derived_epik=dj0yJnU9ZUNKT0VHNkRjUngyYXVLbTBXOGRYU202UzhIRTJMU2ombj1sOGxJVVU5WG9mQjlaZ0Rod2hSQUF3Jm09MSZ0PUFBQUFBRjJKMVhzJnJtPTEmcnQ9QUFBQUFGMkoxWHM; search=6|1571906170734%7Crb%3D2419-Berkeley-Ave-Los-Angeles%252C-CA%252C-90026%26zpid%3D20743600%26disp%3Dmap%26mdm%3Dauto%26sort%3Ddays%26fs%3D0%26fr%3D1%26mmm%3D0%26rs%3D0%26ah%3D0%26singlestory%3D0%26abo%3D0%26garage%3D0%26pool%3D0%26ac%3D0%26waterfront%3D0%26finished%3D0%26unfinished%3D0%26cityview%3D0%26mountainview%3D0%26parkview%3D0%26waterview%3D0%26hoadata%3D1%09%01%09%09%09%09%090%09US_%09; AWSALB=W62rEtGApa8KRMZPcfAL+7Qnkrh0YRkDL+tQRgaUt4Ylu9+fai2kk0OJZcrePNOIysgIQWeYYh/JPWguJWoNY3+ZwOrNQCvaKYmhCnvxfiXMYwFuJDTSH7/I9ml9',
        'referer' : 'https://www.zillow.com/',
        'cache-control': 'max-age=0',
        'upgrade-insecure-requests': '1',
        'sec-fetch-mode': 'navigate',
        'sec-fetch-site': 'same-origin',
        'sec-fetch-user': '?1',
        'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'}   
    }

    load(url){
        this.request = rp(url, {
            headers: this.headers
        })
        .then(html => {
            this.html = html;
            return true;
        })
        .catch(error => {
            console.log(error)
            return false;
        })
        return this;
    }

        // queryId - в теории надо будет обновлять
    post(zip, queryId){
        let qdata = `{"operationName":"OffMarketFullRenderQuery","variables":{"zpid":${zip},"contactFormRenderParameter":{"zpid":${zip},"platform":"desktop","isDoubleScroll":false}},"clientVersion":"home-details/5.45.6.6.master.6767331","queryId":"4d1c03e780233dfc5bb6938582cdade1"}`; 
        let data = '';
        var options = {  
            host: 'www.zillow.com',  
            port: 443,  
            path: '/graphql/',  
            method: 'POST',
            headers: {
                'Content-Type' : 'text/plain',
                'Content-Length': Buffer.byteLength(qdata),
                'sec-fetch-mode': 'cors',
                'cache-control': 'no-cache',
                origin: 'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop',
                'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36',
                'postman-token': '936eb338-2995-3371-8485-74f104ed1046',
                accept: '*/*',
                'sec-fetch-site': 'cross-site',
                'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7'
            }
        }
        const prom = new Promise((resolve, reject) => {
            let req = https.request(options, function(res) {
                res.on('data', function(d) {
                    data += d;
                });
                res.on('end', () => {
                    let jdata = JSON.parse(data);
                    resolve(jdata.data.property.forecast);
                });
            });
            req.write(qdata);
            req.end();
        })
        return prom;
    }
}
module.exports = LoadZillow;