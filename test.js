const rp = require('request-promise');
const cheerio = require('cheerio');
const url = 'https://www.zillow.com/homes/2419-Berkeley-Ave-Los-Angeles,-CA,-90026_rb/20743600_zpid/';

// TODO: настроить получение валидных кук
const headers = {'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
               //'accept-encoding': 'gzip, deflate, sdch, br',
               'accept-language': 'en-GB,en;q=0.8,en-US;q=0.6,ml;q=0.4',
               'cookie': 'zguid=23|%24917cefa9-1483-4104-b710-99b0323a7526; zgsession=1|60b831e5-9b52-4a79-a15b-5f8eb913d0ef; _ga=GA1.2.248125237.1569313922; _gid=GA1.2.1416099955.1569313922; zjs_user_id=null; zjs_anonymous_id=%22917cefa9-1483-4104-b710-99b0323a7526%22; JSESSIONID=6A1059F4AF125B4ECFF526D8D4DFA4C0; _gcl_au=1.1.1266799511.1569313925; KruxPixel=true; DoubleClickSession=true; KruxAddition=true; ki_r=; __gads=ID=0c7a4715cda2ba55:T=1569314047:S=ALNI_MaOZ0qPww_oH5eVwDDTht_bOEHkSQ; _pxff_tm=1; ki_s=199445%3A0.0.0.0.2; zgcus_aeuut=AEUUT_2ed3ca7e-dea6-11e9-bba8-0e440c55d788; zgcus_aeut=AEUUT_2ed3ca7e-dea6-11e9-bba8-0e440c55d788; _csrf=Kl2e6wMP_N_PrDujMts-Iqp3; __stripe_sid=3a6499e5-8b00-4c06-8951-fe86cba8d9f1; __stripe_mid=66ca17aa-96d4-4a8d-b853-ca54b7036069; FSsampler=1424989923; ki_t=1569313965806%3B1569313965806%3B1569314016662%3B1%3B2; ZG_SW_REGISTERED=true; rental_csrftoken=97uGAPPm-VGLYTa26ZnUu3b0QxS3PdpvdSK0; swVersion=0.0.339; _gat=1; _px3=6a6f9ab8fad37bbb518348dbbee0fe1b288fc8607a7cc4b69a430c67f444e750:nEC8yKy1V65hvn0xogp1zQuHCWK43W7JtDGGociA63/0YiRuqNoEg6E1qWfDKlnpOvBMnBAdBfFS9xVM7tKJIQ==:1000:pSfmgmwzPBRIv3F5pTnWr0N9U98/MHaSi1rqC/B7VcEhHVd3JpUng1wdHtnia6aNSG3i7J+vTScAaHEgk1vKK8aSVGLPsaJcsylZ77LEksT49uF0YGEPvyFbeb7u9iDO1KPavwSE4EePHFSW6WplGZh6V6m0AnkPbYaqzcRdOek=; _derived_epik=dj0yJnU9ZUNKT0VHNkRjUngyYXVLbTBXOGRYU202UzhIRTJMU2ombj1sOGxJVVU5WG9mQjlaZ0Rod2hSQUF3Jm09MSZ0PUFBQUFBRjJKMVhzJnJtPTEmcnQ9QUFBQUFGMkoxWHM; search=6|1571906170734%7Crb%3D2419-Berkeley-Ave-Los-Angeles%252C-CA%252C-90026%26zpid%3D20743600%26disp%3Dmap%26mdm%3Dauto%26sort%3Ddays%26fs%3D0%26fr%3D1%26mmm%3D0%26rs%3D0%26ah%3D0%26singlestory%3D0%26abo%3D0%26garage%3D0%26pool%3D0%26ac%3D0%26waterfront%3D0%26finished%3D0%26unfinished%3D0%26cityview%3D0%26mountainview%3D0%26parkview%3D0%26waterview%3D0%26hoadata%3D1%09%01%09%09%09%09%090%09US_%09; AWSALB=W62rEtGApa8KRMZPcfAL+7Qnkrh0YRkDL+tQRgaUt4Ylu9+fai2kk0OJZcrePNOIysgIQWeYYh/JPWguJWoNY3+ZwOrNQCvaKYmhCnvxfiXMYwFuJDTSH7/I9ml9',
               'referer' : 'https://www.zillow.com/',
               'cache-control': 'max-age=0',
               'upgrade-insecure-requests': '1',
               'sec-fetch-mode': 'navigate',
               'sec-fetch-site': 'same-origin',
               'sec-fetch-user': '?1',
               'upgrade-insecure-requests': 1,
               'user-agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36'}

rp(url, {
    headers: headers
})
.then(function(html){
    const $ = cheerio.load(html);
    let values = {
        cost: [],
        type: [],
        desc: [],
        imgs: []
    }
    // цена. Две штуки, одна для мобилы, вторая на веба, по идее массив надо потом отфильтровать
    $('.ds-home-details-chip .ds-summary-row .ds-price').each(function() {
        values.cost.push($(this).text());
    });
    // Статус. Две штуки, одна для мобилы, вторая на веба, по идее массив надо потом отфильтровать
    $('.ds-status-details').each(function() {
        values.type.push($(this).text());
    })
    // текст
    $('.character-count-truncated').each(function() {
        values.desc.push($(this).text());
    })
    // скрипт с данными 
    $('script#hdpApolloPreloadedData').each(function(){
        // какой-то кэш апи, судя по всему на сервере просто рендерится что-то типа реакта
        let json = JSON.parse(JSON.parse($(this).html()).apiCache)
        Object.keys(json).map(key => {
            // тут в идеале тоже обработку надо, потому что здесь два объекта, но одинаковых вроде как
            if(typeof json[key]['property'] !== 'undefined' && typeof json[key].property.responsivePhotos !== 'undefined'){
                // фоточки!
                json[key].property.responsivePhotos.map(photo => {
                    // много вариантов размеров фоточек, заберемся последнюю в jpg, не трудно поменять на сохранение всех размеров или webm
                    const img = photo.mixedSources.jpeg[photo.mixedSources.jpeg.length - 1].url;
                    if(img){
                        values.imgs.push(img);
                    }
                })
            }
        })
    })
    // массим value заполнен
    console.log(values);
})
.catch(function(err){
    //handle error
    console.log('We have failed', err)
});