const LoadZillow = require('./load');
const fs = require('fs');

const loader = new LoadZillow();
// скорее всего надо будет менять как-то
let queryId = '5d1c03e780233dfc5bb6938582cdade1';
let result = [];
// читаем файл
fs.readFile('list_items_Zillow', 'utf8', (err, data) => {
    let promises = []
    // разобьем его на строки
    data.split("\n").map(line => {
        // выдеренем номер
        const match = line.match(/\/([0-9]*)_zpid/);
        if(match.length > 1){
            const zip = match[1];
            // как-то криво-косо симулируем задержку между 500 и 2000 мс
            const randowDelay = Math.random() * (2000 - 500) + 500;
            promises.push(
                new Promise((resolve, reject) => {
                    setTimeout(() => {
                        // получим эстимейт
                        loader.post(zip, queryId).then(esimated => {                            
                            result.push(
                                {
                                    zpid: zip,
                                    photos: [],
                                    forecast1yearForZillow: esimated
                                }
                            )
                            resolve();
                        })                
                    }, randowDelay);
                })                
            )
        }
    })
    Promise.all(promises).then(() => {
        // выдача результата
        console.log(result)
    })
})